rust-reqwest (0.11.11-3) unstable; urgency=medium

  * Team upload.
  * Package reqwest 0.11.11 from crates.io using debcargo 2.5.0
  * Re-enable proper rustls support (on architectures were rustls is available)
    now that hyper-rustls and tokio-rustls are packaged. (Closes: #1013869)
  * Don't include provides or autopkgtests for internal features, limit them
    to advertised features.
  * Add README.Debian documenting the feature situation
  * Disable connect_timeout test on s390x, it always seems to fail there.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 07 Jul 2022 22:21:25 +0000

rust-reqwest (0.11.11-2) unstable; urgency=medium

  * Team upload.
  * Package reqwest 0.11.11 from crates.io using debcargo 2.5.0
  * Disable a test that sometimes fails on CI, I think it may have a race
    condition
  * Add architecture restrictions to "all features" autopkgtest.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 26 Jun 2022 21:37:11 +0000

rust-reqwest (0.11.11-1) unstable; urgency=medium

  * Team upload.
  * Package reqwest 0.11.11 from crates.io using debcargo 2.5.0 (Closes: 984864)
  * Drop old patches, no longer relavent.
  * Switch rustl-tls feature to use native-certs instead of webpki-roots.
    (it's disabled right now anway, but if it's re-enabled then native-certs
    seems more appropriate for Debian than webpki-roots).
  * Disable features/optional dependencies that are not satisfiable in Debian.
  * Adjust dev-dependency versions to what is available in Debian.
  * Remove wasm and windows-specific dependencies.
  * Fix a test that fails with a file not found error in the autopkgtest
    environment.
  * Mark tests for the internal "__tls" feature as broken.
  * Set collapse_features = true
  * Limit architectures for rustls related dependencies, provides and
    autopkgtests to architectures where rustls is available.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 26 Jun 2022 01:35:26 +0000

rust-reqwest (0.9.19-5) unstable; urgency=medium

  * Team upload.
  * Package reqwest 0.9.19 from crates.io using debcargo 2.4.4-alpha.0
  * Update the base64 dependencies to unbreak this package
    and spotify-tui (Closes: #992767

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 29 Aug 2021 11:53:46 +0200

rust-reqwest (0.9.19-4) unstable; urgency=medium

  * Team upload.
  * Package reqwest 0.9.19 from crates.io using debcargo 2.4.3
  * Bump base64 dep to 0.12

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 04 Oct 2020 22:02:24 +0200

rust-reqwest (0.9.19-3) unstable; urgency=medium

  * Team upload.
  * Package reqwest 0.9.19 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Tue, 31 Dec 2019 05:56:07 +0000

rust-reqwest (0.9.19-2) unstable; urgency=medium

  * Team upload.
  * Package reqwest 0.9.19 from crates.io using debcargo 2.4.0
  * No need to exclude or clean Cargo.lock with dh-cargo >= 20.

 -- Ximin Luo <infinity0@debian.org>  Tue, 24 Dec 2019 17:53:08 +0000

rust-reqwest (0.9.19-1) unstable; urgency=medium

  * Package reqwest 0.9.19 from crates.io using debcargo 2.2.10

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 13 Aug 2019 15:20:02 +0200
